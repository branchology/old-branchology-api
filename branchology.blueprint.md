FORMAT: 1A

# Branchology API

> Branchology is a family tree management app built for the modern web.

# Group Sources

Sources are records of truth that provide historical information about people.

# Sources [/sources]

## Source List [GET]

Retrieve a paginated collection of sources.

+ Request 
    + Headers 
          Authorization: Bearer 8675309

+ Response 200 (application/json)
  
        {
            "count": 2,
            "total": 2,
            "_links": {
                "self": {
                    "href": "/sources?page=1"
                }
            },
            "_embedded": {
                "sources": [
                    {
                        "id": "a23fb",
                        "title": "The Grand Rapids Press"
                    },
                    {
                        "id": "ec57a",
                        "title": "1940 US Census"
                    }
                ]
            }
        }
