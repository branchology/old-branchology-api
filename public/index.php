<?php

use Branchology\Api\Http\Request;

$app = require '../app/app.php';
$app->run(Request::createFromGlobals());
