<?php

return [
    'driver' => 'pdo_pgsql',
    'host' => 'localhost',
    'dbname' => 'branchology',
    'user' => 'travis',
    'password' => '',
    'dev' => true
];
