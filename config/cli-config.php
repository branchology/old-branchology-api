<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Branchology\Persistence\Command\LoadFixtures;
use Doctrine\DBAL\Migrations\Tools\Console\Command;
use Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Helper\DialogHelper;
use Symfony\Component\Console\Helper\QuestionHelper;

$dbFile = getenv('TESTING') ? 'db.test.php' : 'db.local.php';

if (!file_exists(__DIR__ . '/' .  $dbFile)) {
    die('Please setup db.local.php before continuing');
}

$dbConfig = require __DIR__ . '/' . $dbFile;
$paths = ['src/Persistence/Mapping' => 'Branchology\Domain\Entity'];

$configuration =  Setup::createConfiguration($dbConfig['dev']);
$configuration->setMetadataDriverImpl(new SimplifiedYamlDriver($paths));
$entityManager = EntityManager::create($dbConfig, $configuration);

$helperSet = ConsoleRunner::createHelperSet($entityManager);
$helperSet->set(new QuestionHelper(), 'dialog');

$commands = [
    new Command\DiffCommand(),
    new Command\ExecuteCommand(),
    new Command\GenerateCommand(),
    new Command\MigrateCommand(),
    new Command\StatusCommand(),
    new Command\VersionCommand(),
    new LoadFixtures()
];

return $helperSet;
