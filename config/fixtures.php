<?php

return [
    __DIR__ . '/../fixtures/sources.yml',
    __DIR__ . '/../fixtures/users.yml',
    __DIR__ . '/../fixtures/tokens.yml',
    __DIR__ . '/../fixtures/Place.yml',
    __DIR__ . '/../fixtures/Event.yml',
    __DIR__ . '/../fixtures/Relationship.yml',
    __DIR__ . '/../fixtures/Person.yml',
];
