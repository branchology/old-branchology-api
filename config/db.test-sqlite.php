<?php

return [
    'driver' => 'pdo_sqlite',
    'path' => __DIR__ . '/../tmp/tests.db',
    'dev' => true
];
