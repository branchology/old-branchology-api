<?php

use Branchology\Api\Controller\TokenController;
use Branchology\Api\View\Token as TokenModel;
use Branchology\Domain\Entity\Token as TokenEntity;

describe('Api\Controller\TokenController', function () {
    beforeEach(function () {
        $this->controller = new TokenController();
    });

    describe('get()', function () {
        it('should return a Token model', function () {
            $result = $this->controller->get(new TokenEntity());
            expect($result)->to->be->instanceof(TokenModel::class);
        });

        it('should hydrate the source model with the supplied source', function () {
            $token = (new TokenEntity())
                ->setId('a34f1')
                ->setToken('Foo');
            $result = $this->controller->get($token);
            expect($result->getEntity())->to->equal($token);
        });
    });
});
