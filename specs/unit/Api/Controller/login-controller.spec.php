<?php

use Branchology\Api\Controller\LoginController;
use Branchology\Api\Http\Request;
use Branchology\Api\View\Token as TokenModel;
use Branchology\Domain\Entity\Token as TokenEntity;
use Branchology\Domain\Entity\User;
use Prophecy\Prophet;
use Symfony\Component\HttpFoundation\Response;

describe('Api\Controller\LoginController', function () {
    beforeEach(function () {
        $prophet = new Prophet();
        $this->query = $prophet->prophesize('Branchology\Domain\Query\UserQuery');
        $this->factory = $prophet->prophesize('Branchology\Domain\Service\TokenFactory');
        $this->controller = new LoginController(
            $this->query->reveal(),
            $this->factory->reveal()
        );
    });

    describe('login()', function () {
        context('When the credentials are valid', function () {
            it('should return a Token model', function () {
                $user = (new User())
                    ->setId(1919)
                    ->setPassword('$2y$10$izXEeLHgGfmWr850U67/IeJVCbC97DoPnZbg.a0sJmIvhE7Y1vL0a');
                $token = (new TokenEntity())->setId(444);

                $this->query->whereEmail('foo')->willReturn($this->query);
                $this->query->fetchOne()->willReturn($user);

                $this->factory->createForUser($user)->willReturn($token);

                $result = $this->controller->login(new Request([], ['email' => 'foo', 'password' => 'cheese']));
                expect($result)->to->be->instanceof(TokenModel::class);
                expect($result->getEntity())->to->equal($token);
            });
        });

        context('When the credentials are invalid', function () {
            it('should return a 422 response', function () {
                $this->query->whereEmail('foo')->willReturn($this->query);
                $this->query->fetchOne()->willReturn(null);

                $result = $this->controller->login(new Request([], ['email' => 'foo']));

                expect($result)->to->be->instanceof(Response::class);
                expect($result->getStatusCode())->to->equal(422);
            });
        });
    });
});
