<?php

use Branchology\Api\Controller\PeopleController;
use Branchology\Api\View\PeopleCollection;
use Branchology\Api\View\Person as PersonModel;
use Branchology\Domain\Entity\Person as PersonEntity;
use Branchology\Domain\Query\PeopleQuery;
use Symfony\Component\HttpFoundation\Request;

describe('Api\Controller\PeopleController', function () {
    beforeEach(function () {
        $this->query = $this->getProphet()->prophesize(PeopleQuery::class);
        $this->controller = new PeopleController($this->query->reveal());
    });

    describe('listAll()', function () {
        beforeEach(function () {
            $this->query->total()->willReturn(10);
            $this->query->fetch()->willReturn([
                new PersonEntity(),
                new PersonEntity(),
                new PersonEntity()
            ]);
        });

        it('should return a PeopleCollection', function () {
            expect($this->controller->listAll(Request::create('/foo')))
                ->to->be->instanceof(PeopleCollection::class);
        });

        it('should embed the items returned by the query', function () {
            $result = $this->controller->listAll(Request::create('/foo'));
            expect($result->render()['_embedded']['people'])->to->have->length(3);
        });

        it('should respect the perPage param in the request', function () {
            $this->query->limit(8)->shouldBeCalled();
            $request = Request::create('/foo', 'GET', ['perPage' => 8]);
            $this->controller->listall($request);
        });

        it('should respect the start param in the request', function () {
            $this->query->start(11)->shouldBeCalled();
            $request = Request::create('/foo', 'GET', ['start' => 11]);
            $this->controller->listall($request);
        });
    });

    describe('get()', function () {
        it('should return a Person model', function () {
            $result = $this->controller->get(new PersonEntity());
            expect($result)->to->be->instanceof(PersonModel::class);
        });

        it('should hydrate the Person model with the supplied Person', function () {
            $person = (new PersonEntity())
                ->setId('a34f1')
                ->setGender('M');
            $result = $this->controller->get($person);
            expect($result->render())->to->have->property('id', 'a34f1');
            expect($result->render())->to->have->property('gender', 'M');
        });
    });

    afterEach(function () {
        $this->getProphet()->checkPredictions();
    });
});
