<?php

use Branchology\Api\Controller\SourceController;
use Branchology\Api\View\Source as SourceModel;
use Branchology\Api\View\SourceCollection;
use Branchology\Domain\Entity\Source as SourceEntity;
use Branchology\Domain\Query\SourceQuery;
use Symfony\Component\HttpFoundation\Request;

describe('Api\Controller\SourceController', function () {
    beforeEach(function () {
        $this->query = $this->getProphet()->prophesize(SourceQuery::class);
        $this->controller = new SourceController($this->query->reveal());
    });

    describe('listAll()', function () {
        beforeEach(function () {
            $this->query->total()->willReturn(10);
            $this->query->fetch()->willReturn([
                new SourceEntity(),
                new SourceEntity(),
                new SourceEntity()
            ]);
        });

        it('should return a SourceCollection', function () {
            expect($this->controller->listAll(Request::create('/foo')))
                ->to->be->instanceof(SourceCollection::class);
        });

        it('should embed the items returned by the query', function () {
            $result = $this->controller->listAll(Request::create('/foo'));
            expect($result->render()['_embedded']['sources'])->to->have->length(3);
        });

        it('should respect the perPage param in the request', function () {
            $this->query->limit(8)->shouldBeCalled();
            $request = Request::create('/foo', 'GET', ['perPage' => 8]);
            $this->controller->listall($request);
        });

        it('should respect the start param in the request', function () {
            $this->query->start(11)->shouldBeCalled();
            $request = Request::create('/foo', 'GET', ['start' => 11]);
            $this->controller->listall($request);
        });
    });

    describe('get()', function () {
        it('should return a Source model', function () {
            $result = $this->controller->get(new SourceEntity());
            expect($result)->to->be->instanceof(SourceModel::class);
        });

        it('should hydrate the source model with the supplied source', function () {
            $source = (new SourceEntity())
                ->setId('a34f1')
                ->setTitle('Foo')
                ->setAuthor('Chuck');
            $result = $this->controller->get($source);
            expect($result->render())->to->have->property('id', 'a34f1');
            expect($result->render())->to->have->property('title', 'Foo');
            expect($result->render())->to->have->property('author', 'Chuck');
        });
    });

    afterEach(function () {
        $this->getProphet()->checkPredictions();
    });
});
