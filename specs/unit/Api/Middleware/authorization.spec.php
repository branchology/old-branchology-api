<?php

use Branchology\Api\Http\Request;
use Branchology\Api\Middleware\Authorization;
use Branchology\Domain\Entity\Token;
use Branchology\Domain\Query\TokenQuery;
use Prophecy\Prophet;
use Symfony\Component\HttpFoundation\Response;

describe('Api\Middleware\Authorization', function () {
    beforeEach(function () {
        $prophet = new Prophet();
        $this->query = $prophet->prophesize(TokenQuery::class);
        $this->middleware = new Authorization($this->query->reveal());
    });

    describe('check()', function () {
        it('should return null when a valid token is passed', function () {
            $this->query->whereToken('JohnnyFive')->willReturn($this->query);
            $this->query->fetchOne()->willReturn(
                (new Token())
                    ->setExpires((new DateTime())->add(new DateInterval('P1D')))
            );

            $request = new Request();
            $request->headers->set('Authorization', 'Bearer JohnnyFive');

            $result = $this->middleware->check($request);
            expect($result)->to->be->null();
        });

        it('should return a 401 when an expired token is passed', function () {
            $this->query->whereToken('JohnnyFive')->willReturn($this->query);
            $this->query->fetchOne()->willReturn(
                (new Token())
                    ->setExpires(new DateTime())
            );

            $request = new Request();
            $request->headers->set('Authorization', 'Bearer JohnnyFive');

            $result = $this->middleware->check($request);
            expect($result)->to->be->instanceof(Response::class);
            expect($result->getStatusCode())->to->equal(401);
        });

        it('should return a 401 when an invalid token is passed', function () {
            $this->query->whereToken('JohnnyFive')->willReturn($this->query);
            $this->query->fetchOne()->willReturn(null);

            $request = new Request();
            $request->headers->set('Authorization', 'Bearer JohnnyFive');

            $result = $this->middleware->check($request);
            expect($result)->to->be->instanceof(Response::class);
            expect($result->getStatusCode())->to->equal(401);
        });

        it('should return a 401 when no authorization header is supplied', function () {
            $result = $this->middleware->check(new Request());
            expect($result)->to->be->instanceof(Response::class);
            expect($result->getStatusCode())->to->equal(401);
        });

        it('should return a 401 when authorization mode is not "bearer"', function () {
            $request = new Request();
            $request->headers->set('Authorization', 'Salad IsGood4U');

            $result = $this->middleware->check($request);
            expect($result)->to->be->instanceof(Response::class);
            expect($result->getStatusCode())->to->equal(401);
        });

        it('should return a 401 when authorization header is just weird', function () {
            $request = new Request();
            $request->headers->set('Authorization', 'Saadsfadsf ads fad ads od4U');

            $result = $this->middleware->check($request);
            expect($result)->to->be->instanceof(Response::class);
            expect($result->getStatusCode())->to->equal(401);
        });
    });
});
