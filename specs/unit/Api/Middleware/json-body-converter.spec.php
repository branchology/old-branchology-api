<?php

use Branchology\Api\Http\Request;
use Branchology\Api\Middleware\JsonBodyConverter;

describe('Api\Middleware\JsonBodyConverter', function () {
    beforeEach(function () {
        $this->bodyConverter = new JsonBodyConverter();
    });

    describe('convert()', function () {
        it('should convert an application/json request to an array of data', function () {
            $request = new Request([], [], [], [], [], [], '{"foo": "bar"}');
            $request->headers->set('Content-Type', 'application/json');
            $this->bodyConverter->convert($request);
            expect($request->request->get('foo'))->to->equal('bar');
        });

        it('should leave all other content types alone', function () {
            $request = new Request([], ['foo' => 'bar']);
            $this->bodyConverter->convert($request);
            expect($request->request->all())->to->equal(['foo' => 'bar']);
        });
    });
});
