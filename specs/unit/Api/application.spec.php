<?php

use Branchology\Api\Application\Application;
use Branchology\Api\Http\Request;
use Symfony\Component\HttpFoundation\Response;

describe('Api\Application', function () {
    beforeEach(function () {
        $this->app = new Application();
    });

    describe('series()', function () {
        it('should return a closure', function () {
            $result = $this->app->series([]);
            expect($result)->to->be->instanceof('Closure');
        });

        it('should execute the callables passed', function () {
            $invokeOne = $invokeTwo = 0;

            $result = $this->app->series([
                function () use (&$invokeOne) {$invokeOne++;},
                function () use (&$invokeTwo) {$invokeTwo++;}
            ]);

            $result(new Request());

            expect($invokeOne)->to->equal(1);
            expect($invokeOne)->to->equal(1);
        });

        it('should pass the returned values of each callable to the next', function () {
            $result = $this->app->series([
                function () {return 10;},
                function ($number) {return new Response($number / 2);}
            ]);

            $value = $result(new Request());
            expect($value->getContent())->to->equal('5');
        });

        it('should short circuit if any callable returns a Response', function () {
            $result = $this->app->series([
                function () {return new Response('first callable');},
                function () {throw new Exception('This should not have been called');}
            ]);
            $response = $result(new Request());
            expect($response)->to->be->instanceof(Response::class);
            expect($response->getContent())->to->equal('first callable');
        });
    });
});
