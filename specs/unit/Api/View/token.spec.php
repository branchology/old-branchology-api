<?php

use Branchology\Api\View\Token as TokenModel;
use Branchology\Domain\Entity\Token;
use Branchology\Domain\Entity\User;

describe('Api\View\Token', function () {
    beforeEach(function () {
        $this->token = new Token();
        $this->token->setToken('abc123')
            ->setCreated(new DateTime())
            ->setExpires((new DateTime())->add(new DateInterval('P30D')))
            ->setUser(
                (new User())
                    ->setId(100)
                    ->setCreated((new DateTime())->sub(new DateInterval('P2M')))
            );
        $this->model = new TokenModel($this->token);
    });

    describe('render()', function () {
        beforeEach(function () {
            $this->data = $this->model->render();
        });

        it('should include the token', function () {
            expect($this->data)->to->have->property('token', 'abc123');
        });

        it('should include an expiration date', function () {
            expect($this->data)->to->have->property('created', $this->token->getCreated()->format('c'));
        });

        it('should embed the user', function () {
            $expected = [
                'id' => $this->token->getUser()->getId(),
                'email' => $this->token->getUser()->getEmail(),
                'created' => $this->token->getUser()->getCreated()->format('c')
            ];

            expect($this->data)->to->have->a->deep->property('_embedded[user][0]');
            expect($this->data['_embedded']['user'][0])->to->equal($expected);
        });

        it('should provide a link for self', function () {
            expect($this->data)->to->have->deep->property(
                '_links[self][href]',
                '/token/' . $this->token->getId()
            );
        });

        it('should provide a link for the user', function () {

            expect($this->data)->to->have->deep->property(
                '_links[user][href]',
                '/user/' . $this->token->getUser()->getId()
            );
        });
    });
});
