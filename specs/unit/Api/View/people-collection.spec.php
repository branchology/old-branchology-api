<?php

use Branchology\Api\View\PeopleCollection;
use Branchology\Domain\Entity\Person;
use Branchology\Domain\Entity\Source;

describe('Api\View\PeopleCollection', function () {
    beforeEach(function () {
        $this->query = $this->getProphet()->prophesize('Branchology\Domain\Query\PeopleQuery');
        $this->model = new PeopleCollection($this->query->reveal());
    });

    describe('->render()', function () {
        beforeEach(function () {
            $this->query->total()->willReturn(17);
            $this->query->fetch()->willReturn([
                new Person(),
                new Person(),
                new Person(),
            ]);
        });

        it('should property set the count', function () {
            $data = $this->model->render();
            expect($data['count'])->to->equal(3);
        });

        it('should property set the total', function () {
            $data = $this->model->render();
            expect($data['total'])->to->equal(17);
        });

        it('should embed the returned resources', function () {
            $data = $this->model->render();
            expect($data['_embedded']['people'])->to->have->length(3);
        });
    });
});
