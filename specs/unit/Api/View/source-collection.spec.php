<?php

use Branchology\Api\View\SourceCollection;
use Branchology\Domain\Entity\Source;

describe('Api\View\SourceCollection', function () {
    beforeEach(function () {
        $this->query = $this->getProphet()->prophesize('Branchology\Domain\Query\SourceQuery');
        $this->model = new SourceCollection($this->query->reveal());
    });

    describe('->render()', function () {
        beforeEach(function () {
            $this->query->total()->willReturn(17);
            $this->query->fetch()->willReturn([
                new Source(),
                new Source(),
                new Source(),
            ]);
        });

        it('should property set the count', function () {
            $data = $this->model->render();
            expect($data['count'])->to->equal(3);
        });

        it('should property set the total', function () {
            $data = $this->model->render();
            expect($data['total'])->to->equal(17);
        });

        it('should embed the returned resources', function () {
            $data = $this->model->render();
            expect($data['_embedded']['sources'])->to->have->length(3);
        });
    });
});
