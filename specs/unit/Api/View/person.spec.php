<?php

use Branchology\Api\View\Person as PersonModel;
use Branchology\Domain\Entity\Event;
use Branchology\Domain\Entity\Person as PersonEntity;
use Branchology\Domain\Entity\Place;

describe('Api\View\Person', function () {
    describe('render()', function () {
        beforeEach(function () {
            $this->person = new PersonEntity();
            $this->model = new PersonModel($this->person);
        });

        it('should include basic person info', function () {
            $this->person->setId('abcdef0123456789');
            $this->person->setGender('F');

            expect($this->model->render())->to->have->property('id', 'abcdef0123456789');
            expect($this->model->render())->to->have->property('gender', 'F');
        });

        context('name', function () {
            it('should include the preferred name, if available', function () {
                $this->person->getNames()->add(
                    (new \Branchology\Domain\Entity\Name('Steve', 'Rogers'))
                        ->setPreferred(true)
                );

                expect($this->model->render())->to->have->deep->property('name->personal', 'Steve Rogers');
                expect($this->model->render())->to->have->deep->property('name->given', 'Steve');
                expect($this->model->render())->to->have->deep->property('name->surname', 'Rogers');
            });

            it('should omit name entirely if not available', function () {
                expect($this->model->render())->to->not->have->property('name');
            });
        });

        context('birth', function () {
            it('should include birth info if available', function () {
                $this->person->getEvents()->add(
                    (new Event())
                        ->setType('BIRT')
                        ->setDate('8 Jun 1855')
                        ->setPlace((new Place())->setDescription('West Rapids, MI'))
                        ->setPreferred(true)
                );
                expect($this->model->render())->to->have->deep->property('birth->date', '8 Jun 1855');
                expect($this->model->render())->to->have->deep->property('birth->place', 'West Rapids, MI');
            });

            it('should omit birth entirely if not available', function () {
                expect($this->model->render())->to->not->have->property('birth');
            });
        });

        context('death', function () {
            it('should include death info if available', function () {
                $this->person->getEvents()->add(
                    (new Event())
                        ->setType('DEAT')
                        ->setDate('26 Dec 1922')
                        ->setPlace((new Place())->setDescription('Outerville, MI'))
                        ->setPreferred(true)
                );
                expect($this->model->render())->to->have->deep->property('death->date', '26 Dec 1922');
                expect($this->model->render())->to->have->deep->property('death->place', 'Outerville, MI');
            });

            it('should omit death entirely if not available', function () {
                expect($this->model->render())->to->not->have->property('death');
            });
        });
    });
});
