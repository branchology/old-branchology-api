<?php

use Branchology\Api\Converter\RepositoryBasedConverter;
use Branchology\Domain\Entity\Source;
use Branchology\Domain\Repository\SourceRepository;
use Prophecy\Prophet;

describe('Api\Converter\RepositoryBasedConverter', function () {
    beforeEach(function () {
        $prophet = new Prophet();
        $this->repository = $prophet->prophesize(SourceRepository::class);
        $this->converter = new RepositoryBasedConverter($this->repository->reveal());
    });

    describe('convert()', function () {
        it('should retrieve the passed ID from the repository', function () {
            $source = (new Source())->setId(22);
            $this->repository->get(5)->willReturn($source);
            $converted = $this->converter->convert(5);

            $this->repository->get(5)->shouldHaveBeenCalled();
            expect($converted)->to->equal($source);
        });
    });
});
