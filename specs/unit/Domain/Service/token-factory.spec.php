<?php

use Branchology\Domain\Entity\Token;
use Branchology\Domain\Entity\User;
use Branchology\Domain\Service\TokenFactory;

describe('Domain\Service\TokenFactory', function () {
    beforeEach(function () {
        $this->factory = new TokenFactory();
    });

    describe('createForUser()', function () {
        it('should create a new token for the given User', function () {
            $user = (new User())->setId(500);
            $token = $this->factory->createForUser($user);

            expect($token)->to->be->instanceof(Token::class);
        });
    });
});
