<?php

use Branchology\Domain\Entity\Source;
use Branchology\Domain\Repository\SourceRepository;
use Branchology\Domain\Service\Hydrator\SourceHydrator;
use Prophecy\Prophet;

describe('Domain\Service\Hydrator\SourceHydrator', function () {
    beforeEach(function () {
        $prophet = new Prophet();
        $this->repository = $prophet->prophesize(SourceRepository::class);
        $this->hydrator = new SourceHydrator($this->repository->reveal());
    });

    describe('hydrate()', function () {
        it('should hydrate the object', function () {
            $object = $this->hydrator->hydrate(['title' => 'Test', 'author' => 'Boo']);
            expect($object->getTitle())->to->equal('Test');
            expect($object->getAuthor())->to->equal('Boo');
        });

        it('should retrieve the object from the repository', function () {
            $this->repository->get(100)->willReturn(new Source());
            $this->hydrator->hydrate(['id' => 100]);
        });
    });
});
