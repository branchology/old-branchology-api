<?php

use Branchology\Domain\Entity\Behavior\Stampable;

describe('Domain\Entity\Behavior\Stampable', function() {
   describe('->setCreated()', function() {
      it('should convert the timezone to UTC', function () {
         $date = new DateTime('2015-03-10 5:00 PM', new \DateTimeZone('EST5EDT'));

         $obj = new TestStampable();
         $obj->setCreated($date);

         expect($obj->getCreated()->getTimezone()->getName())->to->equal('UTC');
         expect($obj->getCreated()->format('H:i'))->to->equal('21:00');
      });
   });
});

class TestStampable {
   use Stampable;
}
