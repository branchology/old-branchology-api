<?php

use Branchology\Domain\Entity\Name;

describe('Domain\Entity\Name', function () {
    describe('__construct()', function () {
        it('should allow setting the name', function () {
            $name = new Name('Carl', 'Jones', 'Jr');

            expect($name->getGiven())->to->equal('Carl');
            expect($name->getSurname())->to->equal('Jones');
            expect($name->getSuffix())->to->equal('Jr');
            expect($name->getPersonal())->to->equal('Carl Jones');
        });

        it('should not set anything if nothing is passed', function () {
            $name = new Name();

            expect($name->getGiven())->to->equal(null);
            expect($name->getSurname())->to->equal(null);
            expect($name->getSuffix())->to->equal(null);
            expect($name->getPersonal())->to->equal('');
        });

        it('should handle various pieces being passed', function () {
            $name = new Name('', 'Jones');
            expect($name->getPersonal())->to->equal('Jones');

            $name = new Name('Carl');
            expect($name->getPersonal())->to->equal('Carl');
        });
    });
});
