<?php

use Branchology\Domain\Entity\Event;
use Branchology\Domain\Entity\Name;
use Branchology\Domain\Entity\Parents;
use Branchology\Domain\Entity\Person;
use Doctrine\Common\Collections\ArrayCollection;

describe('Domain\Entity\Person', function () {
    describe('->getPreferredName()', function () {
        it('should return the name marked preferred', function () {
            $preferred = (new Name('Mike', 'Jones'))->setPreferred(true);
            $names = new ArrayCollection([
                new Name('Bill', 'Jones'),
                new Name('Susan', 'Jones'),
                new Name('Frannie', 'Jones'),
                $preferred,
                new Name('Charles', 'Jones'),
            ]);
            $person = (new Person())->setNames($names);
            expect($person->getPreferredName())->to->equal($preferred);
        });
    });

    describe('->getBirth()', function () {
        it('should return the preferred event of type "BIRT"', function () {
            $preferred = (new Event())->setPreferred(true)->setType('BIRT');
            $events = new ArrayCollection([
                new Event(),
                new Event(),
                $preferred,
                new Event(),
                new Event()
            ]);
            $person = (new Person())->setEvents($events);
            expect($person->getBirth())->to->equal($preferred);
        });
    });

    describe('->getDeath()', function () {
        it('should return the preferred event of type "DEAT"', function () {
            $preferred = (new Event())->setPreferred(true)->setType('DEAT');
            $events = new ArrayCollection([
                new Event(),
                new Event(),
                $preferred,
                new Event(),
                new Event()
            ]);
            $person = (new Person())->setEvents($events);
            expect($person->getDeath())->to->equal($preferred);
        });
    });

    describe('->getPreferredParents()', function () {
        it('should return the parents marked as preferred', function () {
            $preferred = (new Parents())->setPreferred(true);
            $parents = new ArrayCollection([
                new Parents(),
                new Parents(),
                $preferred,
                new Parents()
            ]);

            $person = new Person();
            $person->setParents($parents);

            expect($person->getPreferredParents())->to->equal($preferred);
        });

        it('should only return the first preferred parents if there are multiple', function () {
            $preferred = (new Parents())->setPreferred(true);
            $parents = new ArrayCollection([
                new Parents(),
                new Parents(),
                $preferred,
                (new Parents())->setPreferred(true)
            ]);

            $person = new Person();
            $person->setParents($parents);

            expect($person->getPreferredParents())->to->equal($preferred);
        });

    });
});
