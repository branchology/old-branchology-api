<?php

use Branchology\Domain\Entity\Token;

describe('Domain\Entity\Token', function () {
    describe('isExpired()', function () {
        it('should return true if the token has expired', function () {
            $token = new Token();
            $token->setExpires((new Datetime())->sub(new DateInterval('P1D')));

            expect($token->isExpired())->to->equal(true);
        });

        it('should return false if the token has not expired', function () {
            $token = new Token();
            $token->setExpires((new Datetime())->add(new DateInterval('P1D')));

            expect($token->isExpired())->to->equal(false);
        });
    });
});
