<?php

use Branchology\Domain\Entity\Source;
use Branchology\Persistence\ObjectPersister;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\Prophet;

describe('Persistence\ObjectPersister', function () {
    beforeEach(function () {
        $prophet = new Prophet();
        $this->entityManager = $prophet->prophesize(EntityManagerInterface::class);
        $this->persister = new ObjectPersister($this->entityManager->reveal());
    });

    describe('flush()', function () {
        it('should flush the entity manager', function () {
            $this->persister->flush((new Source())->setId('abc123'));
            $this->entityManager->flush()->shouldHaveBeenCalled();
        });

        it('should persist new entities', function () {
            $source = new Source();
            $this->persister->flush($source);
            $this->entityManager->persist($source)->shouldHaveBeenCalled();
        });
    });
});
