<?php

use Branchology\Domain\Entity\Source;
use Branchology\Persistence\Query\AbstractDoctrineQuery;

describe('Persistence\Query\AbstractDoctrineQuery', function () {
    beforeEach(function () {
        $this->query = new TestQuery($this->createEntityManager());
    });

    describe('__construct()', function () {
        it('should throw a RuntimeException with an unconfigured query object', function () {
            $exception = null;

            try {
                new TestUnconfiguredQuery($this->createEntityManager());
            } catch (RuntimeException $e) {
                $exception = $e;
            }

            expect($exception)->to->be->instanceof('RuntimeException');
        });
    });

    describe('getEntityClass()', function () {
        it('should return the class of the configured entity', function () {
            expect($this->query->getEntityClass())->to->equal(Source::class);
        });
    });

    describe('getEntityAlias()', function () {
        it('should return the specified alias of the configured entity', function () {
            expect($this->query->getEntityAlias())->to->equal('source');
        });
    });

    describe('fetch()', function () {
        it('should return the hydrated results', function () {
            $results = $this->query->fetch();
            expect($results)->to->have->length(3);

            foreach ($results as $result) {
                expect($result)->to->be->instanceof(Source::class);
            }
        });
    });

    describe('fetchOne()', function () {
        it('should return one hydrated result', function () {
            $result = $this->query->limit(1)->fetchOne();
            expect($result)->to->be->instanceof(Source::class);
        });
    });

    describe('sort()', function () {
        it('should sort the results as specified', function () {
            $results = $this->query->sort('title', 'desc')->fetch();
            $results = array_map(function ($item) {
                return $item->getTitle();
            }, $results);

            expect($results)->to->equal(['1940 US Census', '1900 US Census', '1850 US Census']);
        });
    });

    describe('start()', function () {
        it('should skip the specified number of results', function () {
            // sqlite requires a limit with offset.......
            $results = $this->query->start(1)->limit(2)->fetch();
            expect($results)->to->have->length(2);
        });
    });

    describe('limit()', function () {
        it('should limit the number of results returned', function () {
            $results = $this->query->limit(1)->fetch();
            expect($results)->to->have->length(1);
        });
    });

    describe('total()', function () {
        it('should return the total rows matching the query', function () {
            $total = $this->query->total();
            expect($total)->to->equal(3);
        });

        context('when start() and limit() are present', function () {
            it('should ignore them and return the total rows', function () {
                $this->query->start(2)->limit(1);
                expect($this->query->total())->to->equal(3);
            });
        });
    });
});

class TestQuery extends AbstractDoctrineQuery
{
    protected $entitySpec = [Source::class => 'source'];
}

class TestUnconfiguredQuery extends AbstractDoctrineQuery
{
}
