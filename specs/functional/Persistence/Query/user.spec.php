<?php

use Branchology\Persistence\Query\UserQuery;

describe('Persistence\Query\UserQuery', function () {
    beforeEach(function () {
        $this->query = new UserQuery($this->createEntityManager());
    });

    describe('whereEmail()', function () {
        it('should return all users matching a given email', function () {
            $users = $this->query->whereEmail('bob@bobcom.com')->fetch();
            expect($users)->to->have->length(1);
        });
    });
});
