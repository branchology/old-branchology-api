<?php

use Branchology\Domain\Entity\Source;
use Branchology\Persistence\Repository\AbstractDoctrineRepository;
use Branchology\Test\FixtureIdentityMap;

describe('Persistence\Repository\AbstractDoctrineRepository', function () {
    beforeEach(function () {
        $this->em = $this->createEntityManager();
        $this->repository = new TestRepository($this->em);
    });

    describe('__construct()', function () {
        it('should throw a RuntimeException with an unconfigured repository object', function () {
            $exception = null;

            try {
                new TestUnconfiguredRepository($this->createEntityManager());
            } catch (RuntimeException $e) {
                $exception = $e;
            }

            expect($exception)->to->be->instanceof('RuntimeException');
        });
    });

    describe('getEntityClass()', function () {
        it('should return the class of the configured entity', function () {
            expect($this->repository->getEntityClass())->to->equal(Source::class);
        });
    });

    describe('getEntityAlias()', function () {
        it('should return the specified alias of the configured entity', function () {
            expect($this->repository->getEntityAlias())->to->equal('source');
        });
    });

    describe('get()', function () {
        it('should retrieve the entity from the EntityManager', function () {
            $id = FixtureIdentityMap::getIdentity('usCensus1900');
            $entity = $this->repository->get($id);
            expect($entity)->to->be->instanceof(Source::class);
            expect($entity->getId())->to->equal($id);
            expect($entity->getTitle())->to->equal('1900 US Census');
        });
    });

    describe('all()', function () {
        it('should retrieve all entities', function () {
            $results = $this->repository->all();
            expect($results)->to->have->length(3);
            foreach ($results as $result) {
                expect($result)->to->be->instanceof(Source::class);
            }
        });

        it('should allow custom sorting', function () {
            $results = $this->repository->all('title', 'asc');
            expect($results[0]->getTitle())->to->equal('1850 US Census');
            expect($results[1]->getTitle())->to->equal('1900 US Census');
            expect($results[2]->getTitle())->to->equal('1940 US Census');
        });

        it('should allow for limiting', function () {
            $results = $this->repository->all(null, null, 2);
            expect($results)->to->have->length(2);
        });

        it('should allow for an offset', function () {
            $results = $this->repository->all(null, null, 2, 2);
            expect($results)->to->have->length(1);
        });
    });
});

class TestRepository extends AbstractDoctrineRepository
{
    protected $entitySpec = [Source::class => 'source'];
}

class TestUnconfiguredRepository extends AbstractDoctrineRepository
{
}
