<?php

use Branchology\Domain\Entity\Person;
use Branchology\Domain\Entity\Relationship;
use Branchology\Test\FixtureIdentityMap;

describe('mapping/person relationships', function () {
    beforeEach(function () {
        $em = $this->createEntityManager();
        $this->repository = $em->getRepository(Person::class);
    });

    it('should properly map a collection of relationships', function () {
        $person = $this->repository->find(FixtureIdentityMap::getIdentity('person1'));
        expect($person->getRelationships())->to->have->length(1);
    });

    it('should properly map the children of a relationship', function () {
        $person = $this->repository->find(FixtureIdentityMap::getIdentity('person1'));
        expect($person->getRelationships()[0]->getChildren())->to->contain($person->getParents()[0]);
    });

    it('should not return any relationships if the person did not have any', function () {
        $person = $this->repository->find(FixtureIdentityMap::getIdentity('personWithNoRelationships'));
        expect($person->getRelationships())->to->have->length(0);
    });

    it('should properly map relationship participants', function () {
        $personId1 = FixtureIdentityMap::getIdentity('person1');
        $personId2 = FixtureIdentityMap::getIdentity('person2');

        $person = $this->repository->find($personId1);
        $participants = $person->getRelationships()[0]->getParticipants();

        $map = function ($participant) {return $participant->getId();};

        expect($participants)->to->have->length(2);

        expect($participants->map($map)->toArray())->to->equal([
            $personId1,
            $personId2
        ]);
    });

    it('should properly map relationship events', function () {
        $person = $this->repository->find(FixtureIdentityMap::getIdentity('person1'));
        $events = $person->getRelationships()[0]->getEvents();

        expect($events)->to->have->length(1);

        expect($events[0]->getType())->to->equal('MARR');
    });

    it('should not return any events if the relationship did not have any', function () {
        $person = $this->repository->find(FixtureIdentityMap::getIdentity('personWithAnUneventfulRelationship'));
        $events = $person->getRelationships()[0]->getEvents();

        expect($events)->to->have->length(0);
    });
});

describe('mapping/person parents', function () {
    beforeEach(function () {
        $em = $this->createEntityManager();
        $this->repository = $em->getRepository(Person::class);
    });

    it('should return the parents of a person', function () {
        $person = $this->repository->find(FixtureIdentityMap::getIdentity('person1'));
        expect($person->getParents())->to->have->length(1);
        expect($person->getParents()[0]->getType())->to->equal('adop');
    });

    it('should attach the parents relationship', function () {
        $person = $this->repository->find(FixtureIdentityMap::getIdentity('person1'));
        $relationship = $person->getParents()[0]->getRelationship();
        expect($relationship)->to->be->instanceof(Relationship::class);
        expect($relationship->getId())->to->equal(FixtureIdentityMap::getIdentity('relationship2'));
    });

    it('should properly link the child', function () {
        $person = $this->repository->find(FixtureIdentityMap::getIdentity('person1'));
        expect($person->getParents()[0]->getChild())->to->equal($person);
    });
});
