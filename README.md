# Branchology API

[![](https://img.shields.io/travis/branchology/branchology-api.svg?style=flat-square)](https://travis-ci.org/branchology/branchology-api)
[![](https://img.shields.io/scrutinizer/g/branchology/branchology-api.svg?style=flat-square)](https://scrutinizer-ci.com/g/branchology/branchology-api)

The API that powers the Branchology project.
