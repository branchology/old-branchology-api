<?php

namespace Branchology\Domain\Service;

use Branchology\Domain\Entity\Token;
use Branchology\Domain\Entity\User;

/**
 * Class TokenFactory
 * @package Branchology\Domain\Service
 */
class TokenFactory
{
    /**
     * @param User $user
     * @return Token
     */
    public function createForUser(User $user)
    {
        $token = new Token();
        $token->setToken(md5($user->getId() . microtime()));
        $token->setExpires((new \DateTime())->add(new \DateInterval('P30D')));
        $token->setCreated(new \DateTime());
        $token->setUser($user);

        return $token;
    }
}
