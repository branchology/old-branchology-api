<?php

namespace Branchology\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Person
 * @package Branchology\Domain\Entity
 */
class Person extends AbstractUuidEntity
{
    /**
     * @var string
     */
    protected $gender;

    /**
     * @var ArrayCollection
     */
    protected $names;

    /**
     * @var ArrayCollection
     */
    protected $events;

    /**
     * @var ArrayCollection
     */
    protected $relationships;

    /**
     * @var ArrayCollection
     */
    protected $parents;

    /**
     * Set us up this person!
     */
    public function __construct()
    {
        $this->names = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->relationships = new ArrayCollection();
        $this->parents = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @return Name
     */
    public function getPreferredName()
    {
        $names = $this->names->filter(function ($name) {
            if ($name->isPreferred()) {
                return true;
            }

            return false;
        });

        return $names->first();
    }

    /**
     * @param ArrayCollection $names
     * @return $this
     */
    public function setNames($names)
    {
        $this->names = $names;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param ArrayCollection $events
     * @return $this
     */
    public function setEvents($events)
    {
        $this->events = $events;
        return $this;
    }

    /**
     * @return Event|null
     */
    public function getBirth()
    {
        return $this->events->filter(function (Event $event) {
            return $event->getType() == 'BIRT' && $event->isPreferred();
        })->first();
    }

    /**
     * @return Event|null
     */
    public function getDeath()
    {
        return $this->events->filter(function (Event $event) {
            return $event->getType() == 'DEAT' && $event->isPreferred();
        })->first();
    }

    /**
     * @return ArrayCollection
     */
    public function getRelationships()
    {
        return $this->relationships;
    }

    /**
     * @param ArrayCollection $relationships
     * @return $this
     */
    public function setRelationships($relationships)
    {
        $this->relationships = $relationships;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * @return null|Parents
     */
    public function getPreferredParents()
    {
        return $this->parents->filter(function (Parents $parents) {
            return $parents->isPreferred();
        })->first();
    }

    /**
     * @param ArrayCollection $parents
     * @return $this
     */
    public function setParents($parents)
    {
        $this->parents = $parents;
        return $this;
    }
}
