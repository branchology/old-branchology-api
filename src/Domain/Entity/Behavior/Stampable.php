<?php

namespace Branchology\Domain\Entity\Behavior;

use DateTime;
use DateTimeZone;

/**
 * Trait Stampable
 * @package Branchology\Domain\Entity\Behavior
 */
trait Stampable
{
    /**
     * @var DateTime
     */
    protected $created;

    /**
     * @var DateTime
     */
    protected $lastModified;

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     * @return $this
     */
    public function setCreated(DateTime $created)
    {
        $created->setTimezone(new DateTimeZone('UTC'));
        $this->created = $created;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * @param DateTime $lastModified
     * @return $this
     */
    public function setLastModified($lastModified)
    {
        $lastModified->setTimezone(new DateTimeZone('UTC'));
        $this->lastModified = $lastModified;

        return $this;
    }
}
