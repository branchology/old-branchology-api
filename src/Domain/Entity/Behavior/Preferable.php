<?php

namespace Branchology\Domain\Entity\Behavior;

/**
 * Trait Preferable
 *
 * @package Branchology\Domain\Entity\Behavior
 */
trait Preferable
{
    /**
     * @var boolean
     */
    protected $preferred;

    /**
     * @return boolean
     */
    public function isPreferred()
    {
        return $this->preferred;
    }

    /**
     * @param boolean $preferred
     * @return $this
     */
    public function setPreferred($preferred)
    {
        $this->preferred = $preferred;
        return $this;
    }
}
