<?php

namespace Branchology\Domain\Entity\Behavior;

use Branchology\Domain\Entity\Citation;

/**
 * Trait Citable
 * @package Branchology\Domain\Entity\Behavior
 */
trait Citable
{
    /**
     * @var array
     */
    protected $citations;

    /**
     * @param Citation $citation
     * @return $this
     */
    public function addCitation(Citation $citation)
    {
        $this->citations[] = $citation;
        return $this;
    }

    /**
     * @return array
     */
    public function getCitations()
    {
        return $this->citations;
    }

    /**
     * @param array $citations
     * @return $this
     */
    public function setCitations($citations)
    {
        $this->citations = $citations;
        return $this;
    }
}
