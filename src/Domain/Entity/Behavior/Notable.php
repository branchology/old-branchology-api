<?php

namespace Branchology\Domain\Entity\Behavior;

use Branchology\Domain\Entity\Note;

/**
 * Trait Notable
 * @package Branchology\Domain\Entity\Behavior
 */
trait Notable
{
    /**
     * @var array
     */
    protected $notes;

    /**
     * @param Note $note
     * @return $this
     */
    public function addNote(Note $note)
    {
        $this->notes[] = $note;
        return $this;
    }

    /**
     * @return array
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param array $notes
     * @return $this
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }
}
