<?php

namespace Branchology\Domain\Entity;

use Branchology\Domain\Entity\Behavior\Citable;
use Branchology\Domain\Entity\Behavior\Notable;
use Branchology\Domain\Entity\Behavior\Preferable;
use Branchology\Domain\Entity\Behavior\Stampable;

/**
 * Class Event
 * @package Branchology\Domain\Entity
 */
class Event extends AbstractUuidEntity
{
    use Citable;
    use Notable;
    use Preferable;
    use Stampable;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $date;

    /**
     * @var string
     */
    protected $dateStamp;

    /**
     * @var Place
     */
    protected $place;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateStamp()
    {
        return $this->dateStamp;
    }

    /**
     * @param string $dateStamp
     * @return $this
     */
    public function setDateStamp($dateStamp)
    {
        $this->dateStamp = $dateStamp;
        return $this;
    }

    /**
     * @return Place
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param Place $place
     * @return $this
     */
    public function setPlace($place)
    {
        $this->place = $place;
        return $this;
    }
}
