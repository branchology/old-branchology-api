<?php

namespace Branchology\Domain\Entity;

/**
 * Class AbstractUuidEntity
 * @package Branchology\Domain\Entity
 */
abstract class AbstractUuidEntity
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
}
