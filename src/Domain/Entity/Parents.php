<?php

namespace Branchology\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Parents
 * @package Branchology\Domain\Entity
 */
class Parents extends AbstractUuidEntity
{
    /**
     * @var Relationship
     */
    protected $relationship;

    /**
     * @var Person
     */
    protected $child;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var boolean
     */
    protected $preferred;

    /**
     * @return ArrayCollection
     */
    public function getRelationship()
    {
        return $this->relationship;
    }

    /**
     * @param ArrayCollection $relationship
     * @return $this
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;
        return $this;
    }

    /**
     * @return Person
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * @param Person $child
     * @return $this
     */
    public function setChild($child)
    {
        $this->child = $child;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @todo make this dynamically determined by type
     * @return boolean
     */
    public function isPreferred()
    {
        return $this->preferred;
    }

    /**
     * @param boolean $preferred
     * @return $this
     */
    public function setPreferred($preferred)
    {
        $this->preferred = $preferred;
        return $this;
    }
}
