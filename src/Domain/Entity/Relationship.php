<?php

namespace Branchology\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Relationship
 * @package Branchology\Domain\Entity
 */
class Relationship extends AbstractUuidEntity
{
    /**
     * @var ArrayCollection
     */
    protected $participants;

    /**
     * @var ArrayCollection
     */
    protected $children;

    /**
     * @var ArrayCollection
     */
    protected $events;

    /**
     * Set us up the class!
     */
    public function __construct()
    {
        $this->participants = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @param ArrayCollection $participants
     * @return $this
     */
    public function setParticipants($participants)
    {
        $this->participants = $participants;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ArrayCollection $children
     * @return $this
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param ArrayCollection $events
     * @return $this
     */
    public function setEvents($events)
    {
        $this->events = $events;
        return $this;
    }
}
