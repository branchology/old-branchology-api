<?php

namespace Branchology\Domain\Entity;

use Branchology\Domain\Entity\Behavior;

/**
 * Class Name
 * @package Branchology\Domain\Entity
 */
class Name extends AbstractUuidEntity
{
    use Behavior\Citable;
    use Behavior\Notable;
    use Behavior\Preferable;

    /**
     * @var Person
     */
    protected $person;

    /**
     * @var string
     */
    protected $personal;

    /**
     * @var string
     */
    protected $prefix;

    /**
     * @var string
     */
    protected $given;

    /**
     * @var string
     */
    protected $surnamePrefix;

    /**
     * @var string
     */
    protected $surname;

    /**
     * @var string
     */
    protected $suffix;

    /**
     * @var string
     */
    protected $nickname;

    /**
     * @param string|null $given
     * @param string|null $surname
     * @param string|null $suffix
     */
    public function __construct($given = null, $surname = null, $suffix = null)
    {
        $this->given = $given;
        $this->surname = $surname;
        $this->suffix = $suffix;

        $this->personal = ($this->given ?: '') .
            ($this->given && $this->surname ? ' ' : '') .
            ($this->surname ?: '');
    }

    /**
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return $this
     */
    public function setPerson($person)
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return string
     */
    public function getPersonal()
    {
        return $this->personal;
    }

    /**
     * @param string $personal
     * @return $this
     */
    public function setPersonal($personal)
    {
        $this->personal = $personal;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     * @return $this
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @return string
     */
    public function getGiven()
    {
        return $this->given;
    }

    /**
     * @param string $given
     * @return $this
     */
    public function setGiven($given)
    {
        $this->given = $given;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurnamePrefix()
    {
        return $this->surnamePrefix;
    }

    /**
     * @param string $surnamePrefix
     * @return $this
     */
    public function setSurnamePrefix($surnamePrefix)
    {
        $this->surnamePrefix = $surnamePrefix;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * @param string $suffix
     * @return $this
     */
    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;
        return $this;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return $this
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
        return $this;
    }
}
