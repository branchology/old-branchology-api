<?php

namespace Branchology\Domain\Repository;

/**
 * Interface EntityRepository
 * @package Branchology\Domain\Repository
 */
interface EntityRepository
{
    /**
     * Retrieve an entity by id
     *
     * @param string $id
     * @return mixed
     */
    public function get($id);

    /**
     * Retrieves all entities.
     *
     * @param string $order
     * @param string $sort
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function all($order = null, $sort = null, $limit = 20, $offset = 0);
}
