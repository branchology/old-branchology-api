<?php

namespace Branchology\Domain\Repository;

/**
 * Interface SourceRepository
 * @package Branchology\Domain\Repository
 */
interface SourceRepository extends EntityRepository
{
}
