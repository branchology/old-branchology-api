<?php

namespace Branchology\Domain\Repository;

/**
 * Interface TokenRepository
 * @package Branchology\Domain\Repository
 */
interface TokenRepository extends EntityRepository
{
}
