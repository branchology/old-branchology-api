<?php

namespace Branchology\Domain\Repository;

/**
 * Interface PersonRepository
 * @package Branchology\Domain\Repository
 */
interface PersonRepository extends EntityRepository
{
}
