<?php

namespace Branchology\Domain\Query;

/**
 * Interface EntityQuery
 * @package Branchology\Domain\Query
 */
interface EntityQuery
{
    /**
     * @param string $id
     * @return mixed
     */
    public function get($id);

    /**
     * Return results starting at the offset of $start.
     *
     * @param int $start
     * @return $this
     */
    public function start($start);

    /**
     * Limit the number of results returned to a max of $limt.
     *
     * @param int $limit
     * @return $this
     */
    public function limit($limit);

    /**
     * Fetch the results described by this Query
     *
     * @return mixed
     */
    public function fetch();

    /**
     * Fetch one result described by this Query.
     *
     * @return mixed
     */
    public function fetchOne();

    /**
     * Returns the total number of results described by this query ignoring any defined limit or start value.
     *
     * @return int
     */
    public function total();
}
