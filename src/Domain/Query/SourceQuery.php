<?php

namespace Branchology\Domain\Query;

/**
 * Interface SourceQuery
 * @package Branchology\Domain\Query
 */
interface SourceQuery extends EntityQuery
{
}
