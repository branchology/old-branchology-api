<?php

namespace Branchology\Domain\Query;

/**
 * Class UserQuery
 * @package Branchology\Domain\Query
 */
interface UserQuery extends EntityQuery
{
    /**
     * @param string $email
     * @return mixed
     */
    public function whereEmail($email);
}
