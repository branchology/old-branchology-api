<?php

namespace Branchology\Domain\Query;

/**
 * Interface PeopleQuery
 * @package Branchology\Domain\Query
 */
interface PeopleQuery extends EntityQuery
{
}
