<?php

namespace Branchology\Domain\Query;

/**
 * Interface TokenQuery
 * @package Branchology\Domain\Query
 */
interface TokenQuery extends EntityQuery
{
    /**
     * @param string $token
     * @return mixed
     */
    public function whereToken($token);
}
