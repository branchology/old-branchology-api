<?php

namespace Branchology\Persistence;

use Branchology\Domain\Entity\AbstractUuidEntity;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ObjectPersister
 * @package Branchology\Persistence
 */
class ObjectPersister
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param AbstractUuidEntity $entity
     * @return AbstractUuidEntity
     */
    public function flush(AbstractUuidEntity $entity)
    {
        if (empty($entity->getId())) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();

        return $entity;
    }
}
