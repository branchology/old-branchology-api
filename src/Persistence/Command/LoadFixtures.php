<?php

namespace Branchology\Persistence\Command;

use Branchology\Test\FixtureIdentityMap;
use Nelmio\Alice\Fixtures;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class LoadFixtures
 * @package Branchology\Persistence\Command
 */
class LoadFixtures extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('alice:load-fixtures')
            ->setDescription('Load the Alice fixtures into the database')
            ->addOption('config', 'c', InputOption::VALUE_REQUIRED, 'The configuration file of fixtures to load');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $input->getOption('config');

        if (!$config) {
            throw new \InvalidArgumentException('--config file is required');
        }

        if (!file_exists($config)) {
            throw new \InvalidArgumentException('Configuration file ' . $config . ' does not exist');
        }

        $objects = Fixtures::load(require $config, $this->getHelper('em')->getEntityManager());

        FixtureIdentityMap::loadIdentityMap($objects);
    }
}
