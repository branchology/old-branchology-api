<?php

namespace Branchology\Persistence\Repository;

use Branchology\Domain\Entity\Source;
use Branchology\Domain\Repository\SourceRepository as SourceRepositoryInterface;

/**
 * Class SourceRepository
 * @package Branchology\Persistence\Repository
 */
class SourceRepository extends AbstractDoctrineRepository implements SourceRepositoryInterface
{
    /**
     * @var array
     */
    protected $entitySpec = [Source::class => 'source'];
}
