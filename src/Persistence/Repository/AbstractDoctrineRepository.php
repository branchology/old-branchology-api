<?php

namespace Branchology\Persistence\Repository;

use Branchology\Domain\Repository\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

/**
 * Class AbstractDoctrineRepository
 * @package Branchology\Persistence\Repository
 */
abstract class AbstractDoctrineRepository implements EntityRepository
{
    /**
     * @var array
     */
    protected $entitySpec;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        if (!class_exists($this->getEntityClass()) || empty($this->getEntityAlias())) {
            throw new RuntimeException('Query is not properly configured');
        }

        $this->entityManager = $entityManager;
    }

    /**
     * @return string
     */
    public function getEntityClass()
    {
        if (!is_array($this->entitySpec) || count($this->entitySpec) < 1) {
            return null;
        }

        return key($this->entitySpec);
    }

    /**
     * @return string
     */
    public function getEntityAlias()
    {
        if (!is_array($this->entitySpec) || count($this->entitySpec) < 1) {
            return null;
        }

        return current($this->entitySpec);
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Retrieve an entity by id
     *
     * @param string $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->getEntityManager()->find($this->getEntityClass(), $id);
    }

    /**
     * {@inheritdoc}
     */
    public function all($order = null, $sort = null, $limit = 20, $offset = 0)
    {
        return $this->getEntityManager()->getRepository($this->getEntityClass())->findBy([], null, $limit, $offset);
    }
}
