<?php

namespace Branchology\Persistence\Repository;

use Branchology\Domain\Entity\Person;
use Branchology\Domain\Repository\PersonRepository as PersonRepositoryInterface;

/**
 * Class PersonRepository
 * @package Branchology\Persistence\Repository
 */
class PersonRepository extends AbstractDoctrineRepository implements PersonRepositoryInterface
{
    /**
     * @var array
     */
    protected $entitySpec = [Person::class => 'person'];
}
