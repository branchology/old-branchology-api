<?php

namespace Branchology\Persistence\Repository;

use Branchology\Domain\Entity\Token;
use Branchology\Domain\Repository\TokenRepository as TokenRepositoryInterface;

/**
 * Class TokenRepository
 * @package Branchology\Persistence\Repository
 */
class TokenRepository extends AbstractDoctrineRepository implements TokenRepositoryInterface
{
    /**
     * @var array
     */
    protected $entitySpec = [Token::class => 'token'];
}
