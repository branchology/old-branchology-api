<?php

namespace Branchology\Persistence\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150604142621 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $events = $schema->createTable('events');
        $events->addColumn('id', 'guid');
        $events->addColumn('place_id', 'guid', ['notnull' => false]);
        $events->addColumn('type', 'string', ['length' => 4]);
        $events->addColumn('date', 'string', ['length' => 35, 'notnull' => false]);
        $events->addColumn('date_stamp', 'string', ['length' => 8, 'notnull' => false]);
        $events->addColumn('is_preferred', 'boolean');

        $events->setPrimaryKey(['id']);
        $events->addForeignKeyConstraint('places', ['place_id'], ['id']);

        $eventSources = $schema->createTable('event_sources');
        $eventSources->addColumn('event_id', 'guid');
        $eventSources->addColumn('source_id', 'guid');
        $eventSources->addUniqueIndex(['source_id']);

        $eventSources->setPrimaryKey(['event_id', 'source_id']);
        $eventSources->addForeignKeyConstraint('events', ['event_id'], ['id']);
        $eventSources->addForeignKeyConstraint('sources', ['source_id'], ['id']);

        $eventNotes = $schema->createTable('event_notes');
        $eventNotes->addColumn('event_id', 'guid');
        $eventNotes->addColumn('note_id', 'guid');
        $eventNotes->addUniqueIndex(['note_id']);

        $eventNotes->setPrimaryKey(['event_id', 'note_id']);
        $eventNotes->addForeignKeyConstraint('events', ['event_id'], ['id']);
        $eventNotes->addForeignKeyConstraint('notes', ['note_id'], ['id']);

        $personEvents = $schema->createTable('person_events');
        $personEvents->addColumn('person_id', 'guid');
        $personEvents->addColumn('event_id', 'guid');
        $personEvents->addUniqueIndex(['event_id']);

        $personEvents->setPrimaryKey(['person_id', 'event_id']);
        $personEvents->addForeignKeyConstraint('people', ['person_id'], ['id']);
        $personEvents->addForeignKeyConstraint('events', ['event_id'], ['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('event_sources');
        $schema->dropTable('event_notes');
        $schema->dropTable('person_events');
        $schema->dropTable('events');
    }
}
