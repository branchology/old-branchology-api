<?php

namespace Branchology\Persistence\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150531165938 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $places = $schema->createTable('places');
        $places->addColumn('id', 'guid');
        $places->addColumn('description', 'string');
        $places->addColumn('street', 'string', ['notnull' => false]);
        $places->addColumn('street2', 'string', ['notnull' => false]);
        $places->addColumn('city', 'string', ['length' => 60, 'notnull' => false]);
        $places->addColumn('state_province', 'string', ['length' => 60, 'notnull' => false]);
        $places->addColumn('postal_code', 'string', ['length' => 20, 'notnull' => false]);
        $places->addColumn('country', 'string', ['length' => 60, 'notnull' => false]);

        $places->setPrimaryKey(['id']);

        $sources = $schema->createTable('sources');
        $sources->addColumn('id', 'guid');
        $sources->addColumn('title', 'string');
        $sources->addColumn('author', 'string', ['notnull' => false]);
        $sources->addColumn('publication', 'string', ['notnull' => false]);
        $sources->addColumn('abbr', 'string', ['length' => 60, 'notnull' => false]);
        $sources->addColumn('text', 'text', ['notnull' => false]);
        $sources->addColumn('rin', 'string', ['length' => 12, 'notnull' => false]);

        $sources->setPrimaryKey(['id']);

        $notes = $schema->createTable('notes');
        $notes->addColumn('id', 'guid');

        $notes->setPrimaryKey(['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('places');
        $schema->dropTable('sources');
        $schema->dropTable('notes');
    }
}
