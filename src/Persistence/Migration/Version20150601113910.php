<?php

namespace Branchology\Persistence\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150601113910 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $people = $schema->createTable('people');
        $people->addColumn('id', 'guid');
        $people->addColumn('gender', 'string', ['length' => 1]);

        $people->setPrimaryKey(['id']);

        $names = $schema->createTable('names');
        $names->addColumn('id', 'guid');
        $names->addColumn('person_id', 'guid');
        $names->addColumn('personal', 'string', ['length' => 120]);
        $names->addColumn('prefix', 'string', ['length' => 30, 'notnull' => false]);
        $names->addColumn('given', 'string', ['length' => 120, 'notnull' => false]);
        $names->addColumn('surname_prefix', 'string', ['length' => 30, 'notnull' => false]);
        $names->addColumn('surname', 'string', ['length' => 120, 'notnull' => false]);
        $names->addColumn('suffix', 'string', ['length' => 30, 'notnull' => false]);
        $names->addColumn('nickname', 'string', ['length' => 30, 'notnull' => false]);
        $names->addColumn('is_preferred', 'boolean');

        $names->setPrimaryKey(['id']);
        $names->addForeignKeyConstraint('people', ['person_id'], ['id']);

        $nameSources = $schema->createTable('name_sources');
        $nameSources->addColumn('name_id', 'guid');
        $nameSources->addColumn('source_id', 'guid');
        $nameSources->addUniqueIndex(['source_id']);

        $nameSources->setPrimaryKey(['name_id', 'source_id']);
        $nameSources->addForeignKeyConstraint('names', ['name_id'], ['id']);
        $nameSources->addForeignKeyConstraint('sources', ['source_id'], ['id']);

        $nameNotes = $schema->createTable('name_notes');
        $nameNotes->addColumn('name_id', 'guid');
        $nameNotes->addColumn('note_id', 'guid');
        $nameNotes->addUniqueIndex(['note_id']);

        $nameNotes->setPrimaryKey(['name_id', 'note_id']);
        $nameNotes->addForeignKeyConstraint('names', ['name_id'], ['id']);
        $nameNotes->addForeignKeyConstraint('notes', ['note_id'], ['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('name_notes');
        $schema->dropTable('name_sources');
        $schema->dropTable('names');
        $schema->dropTable('people');
    }
}
