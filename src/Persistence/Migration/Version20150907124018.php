<?php

namespace Branchology\Persistence\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150907124018 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $relationships = $schema->createTable('relationships');
        $relationships->addColumn('id', 'guid');

        $relationships->setPrimaryKey(['id']);

        $events = $schema->createTable('relationship_events');
        $events->addColumn('relationship_id', 'guid');
        $events->addColumn('event_id', 'guid');

        $events->setPrimaryKey(['relationship_id', 'event_id']);
        $events->addForeignKeyConstraint('relationships', ['relationship_id'], ['id']);
        $events->addForeignKeyConstraint('events', ['event_id'], ['id']);
        $events->addUniqueIndex(['event_id']);

        $personRelationships = $schema->createTable('person_relationships');
        $personRelationships->addColumn('person_id', 'guid');
        $personRelationships->addColumn('relationship_id', 'guid');

        $personRelationships->setPrimaryKey(['person_id', 'relationship_id']);
        $personRelationships->addForeignKeyConstraint('people', ['person_id'], ['id']);
        $personRelationships->addForeignKeyConstraint('relationships', ['relationship_id'], ['id']);

        $personParents = $schema->createTable('person_parents');
        $personParents->addColumn('id', 'guid');
        $personParents->addColumn('relationship_id', 'guid');
        $personParents->addColumn('child_id', 'guid');
        $personParents->addColumn('type', 'string', ['length' => 20]);

        $personParents->setPrimaryKey(['id']);
        $personParents->addForeignKeyConstraint('relationships', ['relationship_id'], ['id']);
        $personParents->addForeignKeyConstraint('people', ['child_id'], ['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('person_parents');
        $schema->dropTable('relationship_events');
        $schema->dropTable('person_relationships');
        $schema->dropTable('relationships');

    }
}
