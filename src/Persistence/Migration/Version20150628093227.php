<?php

namespace Branchology\Persistence\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150628093227 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $users = $schema->createTable('users');
        $users->addColumn('id', 'guid');
        $users->addColumn('email', 'string');
        $users->addColumn('password', 'string');
        $users->addColumn('created', 'datetime');

        $users->setPrimaryKey(['id']);

        $tokens = $schema->createTable('user_tokens');
        $tokens->addColumn('id', 'guid');
        $tokens->addColumn('user_id', 'guid');
        $tokens->addColumn('token', 'string');
        $tokens->addColumn('expires', 'datetime');
        $tokens->addColumn('created', 'datetime');

        $tokens->setPrimaryKey(['id']);
        $tokens->addForeignKeyConstraint('users', ['user_id'], ['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('users');
        $schema->dropTable('user_tokens');
    }
}
