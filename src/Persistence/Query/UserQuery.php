<?php

namespace Branchology\Persistence\Query;

use Branchology\Domain\Entity\User;
use Branchology\Domain\Query\UserQuery as UserQueryInterface;

/**
 * Class UserQuery
 * @package Branchology\Persistence\Query
 */
class UserQuery extends AbstractDoctrineQuery implements UserQueryInterface
{
    /**
     * @var array
     */
    protected $entitySpec = [User::class => 'user'];

    /**
     * @param string $email
     * @return mixed
     */
    public function whereEmail($email)
    {
        $this->query
            ->andWhere($this->getEntityAlias() . '.email = :email')
            ->setParameter('email', $email);

        return $this;
    }
}
