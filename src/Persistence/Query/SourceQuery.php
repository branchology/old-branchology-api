<?php

namespace Branchology\Persistence\Query;

use Branchology\Domain\Entity\Source;
use Branchology\Domain\Query\SourceQuery as SourceQueryInterface;

/**
 * Class SourceQuery
 * @package Branchology\Persistence\Query
 */
class SourceQuery extends AbstractDoctrineQuery implements SourceQueryInterface
{
    /**
     * @var array
     */
    protected $entitySpec = [Source::class => 'source'];
}
