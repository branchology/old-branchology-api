<?php

namespace Branchology\Persistence\Query;

use Branchology\Domain\Entity\Token;
use Branchology\Domain\Query\TokenQuery as TokenQueryInterface;

/**
 * Class TokenQuery
 * @package Branchology\Persistence\Query
 */
class TokenQuery extends AbstractDoctrineQuery implements TokenQueryInterface
{
    /**
     * @var array
     */
    protected $entitySpec = [Token::class => 'token'];

    /**
     * {@inheritdoc}
     */
    public function whereToken($token)
    {
        $this->query
            ->andWhere($this->getEntityAlias() . '.token = :token')
            ->setParameter('token', $token);

        return $this;
    }
}
