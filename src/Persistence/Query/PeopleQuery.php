<?php

namespace Branchology\Persistence\Query;

use Branchology\Domain\Entity\Person;
use Branchology\Domain\Query\PeopleQuery as PeopleQueryInterface;

/**
 * Class PeopleQuery
 * @package Branchology\Persistence\Query
 */
class PeopleQuery extends AbstractDoctrineQuery implements PeopleQueryInterface
{
    /**
     * @var array
     */
    protected $entitySpec = [Person::class => 'person'];
}
