<?php

namespace Branchology\Persistence\Query;

use Branchology\Domain\Query\EntityQuery;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

/**
 * Class AbstractDoctrineQuery
 * @package Branchology\Persistence\Query
 */
abstract class AbstractDoctrineQuery implements EntityQuery
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var array
     */
    protected $entitySpec;

    /**
     * @var \Doctrine\ORM\QueryBuilder
     */
    protected $query;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        if (!class_exists($this->getEntityClass()) || empty($this->getEntityAlias())) {
            throw new RuntimeException('Query is not properly configured');
        }

        $this->entityManager = $entityManager;
        $this->query = $entityManager->createQueryBuilder()
            ->select($this->getEntityAlias())
            ->from($this->getEntityClass(), $this->getEntityAlias());
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityClass()
    {
        if (!is_array($this->entitySpec) || count($this->entitySpec) < 1) {
            return null;
        }

        return key($this->entitySpec);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityAlias()
    {
        if (!is_array($this->entitySpec) || count($this->entitySpec) < 1) {
            return null;
        }

        return current($this->entitySpec);
    }

    /**
     * {@inheritDoc}
     */
    public function get($id)
    {
        $this->query->where($this->getEntityAlias() . ' = :id')
            ->setParameter('id', $id);

        return $this->fetchOne();
    }

    /**
     * {@inheritdoc}
     */
    public function sort($column, $order)
    {
        $this->query->orderBy($this->getEntityAlias() . '.' . $column, $order);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function start($start)
    {
        $this->query->setFirstResult($start);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function limit($limit)
    {
        $this->query->setMaxResults($limit);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function fetch()
    {
        return $this->query->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function fetchOne()
    {
        $result = $this->query->getQuery()->getOneOrNullResult();
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function total()
    {
        $query = clone $this->query;
        $query->setMaxResults(null);
        $query->setFirstResult(null);
        $query->resetDQLPart('select')->addSelect('count(' . $this->getEntityAlias() . ')');

        return (int)$query->getQuery()->getSingleScalarResult();
    }
}
