<?php

namespace Branchology\Persistence;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver;
use Doctrine\ORM\Tools\Setup;

/**
 * Class EntityManagerFactory
 * @package Branchology\Persistence
 */
class EntityManagerFactory
{
    /**
     * @param array $config
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(array $config)
    {
        $paths = [__DIR__ . '/Mapping' => 'Branchology\Domain\Entity'];

        $configuration = Setup::createConfiguration($config['dev']);
        $configuration->setMetadataDriverImpl(new SimplifiedYamlDriver($paths));
        return EntityManager::create($config, $configuration);
    }
}
