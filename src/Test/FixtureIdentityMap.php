<?php

namespace Branchology\Test;

/**
 * Class FixtureIdentityMap
 * @package Branchology\Test
 */
class FixtureIdentityMap
{
    /**
     * @var array
     */
    public static $identityMap = [];

    /**
     * @param array $entities
     */
    public static function loadIdentityMap(array $entities)
    {
        foreach ($entities as $alias => $entity) {
            static::$identityMap[$alias] = $entity->getId();
        }
    }

    /**
     * @param string $alias
     * @return mixed
     */
    public static function getIdentity($alias)
    {
        if (isset(static::$identityMap[$alias])) {
            return static::$identityMap[$alias];
        }

        return null;
    }
}
