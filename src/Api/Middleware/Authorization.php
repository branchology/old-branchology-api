<?php

namespace Branchology\Api\Middleware;

use Branchology\Domain\Query\TokenQuery;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Authorization
 * @package Branchology\Api\Middleware
 */
class Authorization
{
    /**
     * @var TokenQuery
     */
    protected $tokenQuery;

    /**
     * @param TokenQuery $query
     */
    public function __construct(TokenQuery $query)
    {
        $this->tokenQuery = $query;
    }

    /**
     * @param Request $request
     * @return null|Response
     */
    public function check(Request $request)
    {
        if ($request->headers->has('Authorization')) {
            $value = explode(' ', $request->headers->get('Authorization'), 2);

            if (count($value) == 2 && strtolower($value[0]) == 'bearer') {
                $token = $this->tokenQuery->whereToken($value[1])->fetchOne();
                if ($token && !$token->isExpired()) {
                    return null;
                }
            }
        }

        return new Response('', 401);
    }
}
