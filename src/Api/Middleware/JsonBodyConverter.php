<?php

namespace Branchology\Api\Middleware;

use Branchology\Api\Http\Request;

/**
 * Class JsonBodyConverter
 * @package Branchology\Api\Middleware
 */
class JsonBodyConverter
{
    /**
     * @param Request $request
     */
    public function convert(Request $request)
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : []);
        }
    }
}
