<?php

namespace Branchology\Api\View;

use Branchology\Domain\Query\SourceQuery;
use Halpert\Model\AbstractHalResource;

/**
 * Class SourceCollection
 * @package Branchology\Api\View
 */
class SourceCollection extends AbstractHalResource
{
    /**
     * @var SourceQuery
     */
    protected $query;

    /**
     * @var array
     */
    protected $resources = [];

    /**
     * @param SourceQuery $query
     */
    public function __construct(SourceQuery $query)
    {
        $this->query = $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function addEmbeddedResources()
    {
        foreach ($this->resources as $resource) {
            $this->addEmbeddedResource('source', new Source($resource));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function render() : array
    {
        $this->resources = $this->query->fetch();

        return [
            'total' => $this->query->total(),
            'count' => count($this->resources),
        ];
    }

    /**
     * @return string
     */
    public function getSelfPath() : string
    {
        return '/sources';
    }
}
