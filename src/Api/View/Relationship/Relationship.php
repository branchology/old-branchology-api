<?php

namespace Branchology\Api\View\Relationship;

use Branchology\Api\View\Person;
use Branchology\Domain\Entity\Person as PersonEntity;
use Branchology\Domain\Entity\Relationship as RelationshipEntity;
use Halpert\Model\AbstractViewModel;

/**
 * Class Relationship
 * @package Branchology\Api\View\Relationship
 */
class Relationship extends AbstractViewModel
{
    /**
     * @var RelationshipEntity
     */
    protected $resource;

    /**
     * @var string
     */
    protected $rootPersonId;

    /**
     * @param string $rootPersonId
     * @return $this
     */
    public function setRootPersonId(string $rootPersonId)
    {
        $this->rootPersonId = $rootPersonId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSelfPath() : string
    {
        return '/relationship/' . $this->resource->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function addEmbeddedResources()
    {
        if ($this->rootPersonId) {
            $spouse = $this->resource->getParticipants()->filter(function (PersonEntity $person) {
                return $person->getId() !== $this->rootPersonId;
            })->first();

            if ($spouse) {
                $this->addEmbeddedResource('spouse', (new Person($spouse))->setShallow(true));
            }
        }

        foreach ($this->resource->getEvents() as $event) {
            $this->addEmbeddedResource('event', new Event($event));
        }
    }

    /**
     * @return array
     */
    public function render() : array
    {
        return [
            'id' => $this->resource->getId()
        ];
    }
}
