<?php

namespace Branchology\Api\View;

use Branchology\Api\View\People\Event;
use Branchology\Api\View\People\Name;
use Branchology\Api\View\Relationship\Relationship;
use Branchology\Domain\Entity\Person as PersonEntity;
use Halpert\Model\AbstractViewModel;

/**
 * Class Person
 * @package Branchology\Api\View
 */
class Person extends AbstractViewModel
{
    /**
     * @var PersonEntity
     */
    protected $resource;

    /**
     * @return string
     */
    public function getSelfPath() : string
    {
        return '/people/' . $this->resource->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function addEmbeddedResources()
    {
        if (!$this->isShallow()) {
            foreach ($this->resource->getEvents() as $event) {
                $this->addEmbeddedResource('event', new Event($event));
            }

            foreach ($this->resource->getNames() as $name) {
                $this->addEmbeddedResource('name', new Name($name));
            }

            foreach ($this->resource->getRelationships() as $relationship) {
                $relationshipModel = new Relationship($relationship);
                $relationshipModel->setRootPersonId($this->resource->getId());

                $this->addEmbeddedResource('relationship', $relationshipModel);
            }
        }
    }

    /**
     * @return array
     */
    public function render() : array
    {
        $name = $this->resource->getPreferredName();
        $birth = $this->resource->getBirth();
        $death = $this->resource->getDeath();

        $person = [
            'id' => $this->resource->getId(),
            'gender' => $this->resource->getGender()
        ];

        if ($name) {
            $person['name'] = new Name($name);
        }

        if ($birth) {
            $person['birth'] = new Event($birth);
        }

        if ($death) {
            $person['death'] = new Event($death);
        }

        return $person;
    }
}
