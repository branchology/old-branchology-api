<?php

namespace Branchology\Api\View;

use Branchology\Domain\Entity\Token as TokenEntity;
use Halpert\Model\AbstractViewModel;

/**
 * Class Token
 * @package Branchology\Api\View
 */
class Token extends AbstractViewModel
{
    /**
     * @var TokenEntity
     */
    protected $resource;

    /**
     * @return string
     */
    public function getSelfPath() : string
    {
        return '/token/' . $this->resource->getId();
    }

    /**
     * {@inheritdoc}
     */
    protected function addEmbeddedResources()
    {
        $this->addEmbeddedResource('user', new User($this->resource->getUser()));
    }

    /**
     * @return array
     */
    public function render() : array
    {
        return [
            'token' => $this->resource->getToken(),
            'expires' => $this->resource->getExpires()->format('c'),
            'created' => $this->resource->getCreated()->format('c'),
        ];
    }
}
