<?php

namespace Branchology\Api\View;

use Branchology\Domain\Entity\Source as SourceEntity;
use Halpert\Model\AbstractViewModel;

/**
 * Class Source
 * @package Branchology\Api\View
 */
class Source extends AbstractViewModel
{
    /**
     * @var SourceEntity
     */
    protected $resource;

    /**
     * @return string
     */
    public function getSelfPath() : string
    {
        return '/source/' . $this->resource->getId();
    }

    /**
     * @return array
     */
    public function render() : array
    {
        return [
            'id' => $this->resource->getId(),
            'title' => $this->resource->getTitle(),
            'author' => $this->resource->getAuthor(),
            'publication' => $this->resource->getPublication(),
            'abbreviation' => $this->resource->getAbbr(),
            'rin' => $this->resource->getRin(),
            'text' => $this->resource->getText()
        ];
    }
}
