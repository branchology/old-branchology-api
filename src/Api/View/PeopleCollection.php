<?php

namespace Branchology\Api\View;

use Branchology\Domain\Query\PeopleQuery;
use Halpert\Model\AbstractHalResource;

/**
 * Class PeopleCollection
 * @package Branchology\Api\View
 */
class PeopleCollection extends AbstractHalResource
{
    /**
     * @var PeopleQuery
     */
    protected $query;

    /**
     * @var array
     */
    protected $resources = [];

    /**
     * @param PeopleQuery $query
     */
    public function __construct(PeopleQuery $query)
    {
        $this->query = $query;
    }

    /**
     * @return string
     */
    public function getSelfPath() : string
    {
        return '/people';
    }

    /**
     * {@inheritdoc}
     */
    public function addEmbeddedResources()
    {
        foreach ($this->resources as $resource) {
            $this->addEmbeddedResource('person', (new Person($resource))->setShallow(true));
        }
    }

    /**
     * @return array
     */
    public function render() : array
    {
        $this->resources = $this->query->fetch();

        return [
            'total' => $this->query->total(),
            'count' => count($this->resources),
        ];
    }
}
