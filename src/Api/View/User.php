<?php

namespace Branchology\Api\View;

use Halpert\Model\AbstractViewModel;

/**
 * Class User
 * @package Branchology\Api\View
 */
class User extends AbstractViewModel
{
    /**
     * @return string
     */
    public function getSelfPath() : string
    {
        return '/user/' . $this->resource->getId();
    }

    /**
     * @return array
     */
    public function render() : array
    {
        return [
            'id' => $this->resource->getId(),
            'email' => $this->resource->getEmail(),
            'created' => $this->resource->getCreated()->format('c')
        ];
    }
}
