<?php

namespace Branchology\Api\View\People;

use Branchology\Domain\Entity\Name as NameEntity;
use Halpert\Model\AbstractViewModel;

/**
 * Class Name
 * @package Branchology\Api\View\People
 */
class Name extends AbstractViewModel
{
    /**
     * @var NameEntity
     */
    protected $resource;

    /**
     * @return string
     */
    public function getSelfPath() : string
    {
        return '/people/name/' . $this->resource->getId();
    }

    /**
     * @return array
     */
    public function render() : array
    {
        return [
            'id' => $this->resource->getId(),
            'personal' => $this->resource->getPersonal(),
            'given' => $this->resource->getGiven(),
            'surname' => $this->resource->getSurname(),
            'is_preferred' => $this->resource->isPreferred()
        ];
    }
}
