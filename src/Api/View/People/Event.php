<?php

namespace Branchology\Api\View\People;

use Halpert\Model\AbstractViewModel;

/**
 * Class Event
 * @package Branchology\Api\View\People
 */
class Event extends AbstractViewModel
{
    /**
     * @return string
     */
    public function getSelfPath() : string
    {
        return '/person/event/' . $this->resource->getId();
    }

    /**
     * @return array
     */
    public function render() : array
    {
        return [
            'id' => $this->resource->getId(),
            'type' => $this->resource->getType(),
            'date' => $this->resource->getDate(),
            'place' => $this->resource->getPlace()->getDescription(),
        ];
    }
}
