<?php

namespace Branchology\Api\Controller;

use Branchology\Api\Http\Request;
use Branchology\Api\View\PeopleCollection;
use Branchology\Api\View\Person as PersonModel;
use Branchology\Domain\Query\PeopleQuery;
use Halpert\Http\Response\HalJsonResponse;

/**
 * Class PeopleController
 * @package Branchology\Api\Controller
 */
class PeopleController
{
    /**
     * @var PeopleQuery
     */
    protected $peopleQuery;

    /**
     * @param PeopleQuery $peopleQuery
     */
    public function __construct(PeopleQuery $peopleQuery)
    {
        $this->peopleQuery = $peopleQuery;
    }

    /**
     * @param Request $request
     * @return PeopleCollection
     */
    public function getAll(Request $request)
    {
        if ($start = $request->query->get('start')) {
            $this->peopleQuery->start($start);
        }

        if ($perPage = $request->query->get('perPage')) {
            $this->peopleQuery->limit($perPage);
        }

        return new HalJsonResponse(new PeopleCollection($this->peopleQuery));
    }

    /**
     * @param Request $request
     * @return HalJsonResponse
     */
    public function get(Request $request)
    {
        return new HalJsonResponse(new PersonModel($request->getResource()));
    }
}
