<?php

namespace Branchology\Api\Controller;

use Branchology\Api\Http\Request as HttpRequest;
use Branchology\Api\View\Source as SourceModel;
use Branchology\Api\View\SourceCollection;
use Branchology\Domain\Entity\Source as SourceEntity;
use Branchology\Domain\Query\SourceQuery;
use Halpert\Http\Response\HalJsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SourceController
 * @package Branchology\Api\Controller
 */
class SourceController
{
    /**
     * @var SourceQuery
     */
    protected $query;

    /**
     * @param SourceQuery $query
     */
    public function __construct(SourceQuery $query)
    {
        $this->query = $query;
    }

    /**
     * @param Request $request
     * @return SourceCollection
     */
    public function getAll(Request $request)
    {
        if ($start = $request->query->get('start')) {
            $this->query->start($start);
        }

        if ($perPage = $request->query->get('perPage')) {
            $this->query->limit($perPage);
        }

        return new HalJsonResponse(new SourceCollection($this->query));
    }

    /**
     * @param HttpRequest $request
     * @return HalJsonResponse
     */
    public function get(HttpRequest $request)
    {
        return new HalJsonResponse(new SourceModel($request->getResource()));
    }
}
