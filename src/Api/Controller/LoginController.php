<?php

namespace Branchology\Api\Controller;

use Branchology\Api\Http\Request;
use Branchology\Api\View\Token as TokenModel;
use Branchology\Domain\Query\UserQuery;
use Branchology\Domain\Service\TokenFactory;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LoginController
 * @package Branchology\Api\Controller
 */
class LoginController
{
    /**
     * @var UserQuery
     */
    private $query;

    /**
     * @var TokenFactory
     */
    private $tokenFactory;

    /**
     * @param UserQuery $query
     * @param TokenFactory $tokenFactory
     */
    public function __construct(UserQuery $query, TokenFactory $tokenFactory)
    {
        $this->query = $query;
        $this->tokenFactory = $tokenFactory;
    }

    /**
     * @param Request $request
     * @return TokenModel|Response
     */
    public function login(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        $user = $this->query->whereEmail($email)->fetchOne();

        if ($user && password_verify($password, $user->getPassword())) {
            return new TokenModel($this->tokenFactory->createForUser($user));
        }

        // todo fixme validation errors:
        return new Response('', 422);
    }
}
