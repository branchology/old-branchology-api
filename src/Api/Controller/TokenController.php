<?php

namespace Branchology\Api\Controller;

use Branchology\Api\Http\Request;
use Branchology\Api\View\Token as TokenModel;
use Halpert\Http\Response\HalJsonResponse;

/**
 * Class TokenController
 * @package Branchology\Api\Controller
 */
class TokenController
{
    /**
     * @param Request $request
     * @return HalJsonResponse
     */
    public function get(Request $request)
    {
        return new HalJsonResponse(new TokenModel($request->getResource()));
    }
}
