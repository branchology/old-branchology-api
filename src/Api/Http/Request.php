<?php

namespace Branchology\Api\Http;

use Branchology\Domain\Entity\AbstractUuidEntity;
use Symfony\Component\HttpFoundation\Request as HttpRequest;

/**
 * Class Request
 * @package Branchology\Api\Http
 */
class Request extends HttpRequest
{
    /**
     * @var AbstractUuidEntity
     */
    protected $resource;

    /**
     * @return AbstractUuidEntity
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @param AbstractUuidEntity $resource
     * @return $this
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
        return $this;
    }
}
