<?php

namespace Branchology\Api\Hydrator;

use Branchology\Api\Http\Request;
use Branchology\Domain\Repository\EntityRepository;

/**
 * Class AbstractHydrator
 * @package Branchology\Api\Hydrator
 */
abstract class AbstractHydrator
{
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * PeopleHydrator constructor.
     * @param EntityRepository $repository
     */
    public function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     */
    public function fetch(Request $request)
    {
        $resource = $this->repository->get($request->attributes->get('id'));
        $request->setResource($resource);
    }
}
