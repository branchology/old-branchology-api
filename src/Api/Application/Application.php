<?php

namespace Branchology\Api\Application;

use Branchology\Api\Application\Provider\FlushProvider;
use Branchology\Api\Http\Request;
use Silex\Application as SilexApp;
use Silex\Provider\ServiceControllerServiceProvider;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Application
 * @package Branchology\Api\Application
 */
class Application extends SilexApp
{
    /**
     * @param array $values
     */
    public function __construct(array $values = [])
    {
        parent::__construct($values);

        $this->register(new FlushProvider());
        $this->register(new ServiceControllerServiceProvider());
    }

    /**
     * @param callable $module
     * @return mixed
     */
    public function load(callable $module)
    {
        return $module($this);
    }

    /**
     * @param array $routes
     */
    public function mapOptions(array $routes)
    {
        foreach ($routes as $route => $methods) {
            $this->match($route, function () use ($methods) {
                return Response::create(null, 200, ['Allowed' => implode(',', $methods)]);
            })->method('OPTIONS');
        }
    }

    /**
     * Returns a closure that will invoke the passed series of callables when invoked.
     *
     * @param array $series
     * @return \Closure
     */
    public function series(array $series)
    {
        return function (Request $request) use ($series) {
            $lastReturnValue = null;

            foreach ($series as $step) {
                $callback = $this['callback_resolver']->resolveCallback($step);
                $lastReturnValue = $callback($lastReturnValue ?: $request);

                if ($lastReturnValue instanceof Response) {
                    return $lastReturnValue;
                }
            }
        };
    }
}
