<?php

namespace Branchology\Api\Application\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent as ResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class FlushProvider
 * @package Branchology\Api\Application\Provider
 */
class FlushProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $application)
    {
//        $application['dispatcher']->addListener(KernelEvents::VIEW, function (ResultEvent $event) use ($application) {
//            $em = $application['orm.entity-manager'];
//            if ($event->getControllerResult() instanceof AbstractViewModel) {
//                if ($event->getRequest()->getMethod() == 'POST') {
//                    $em->persist($event->getControllerResult()->getEntity());
//                    $em->flush();
//                } elseif (in_array($event->getRequest()->getMethod(), ['PUT', 'PATCH'])) {
//                    $em->flush();
//                }
//            }
//        });
    }

    public function boot(Application $application)
    {
    }
}
