<?php

use Branchology\Api\Application\Application;

return function (Application $app) {
    // Auth
    $app->post('/login', 'login.controller:login');
    $app->get('/token/{id}', 'token.controller:get')
        ->before('token.hydrator:fetch');

    // Sources
    $app->get('/sources', 'source.controller:getAll')
        ->before('authorization.service:check');

    $app->get('/sources/{id}', 'source.controller:get')
        ->before('source.hydrator:fetch')
        ->before('authorization.service:check');

    // People
    $app->get('/people', 'people.controller:getAll')
        ->before('authorization.service:check');

    $app->get('/people/{id}', 'people.controller:get')
        ->before('person.hydrator:fetch')
        ->before('authorization.service:check');

    // Handle OPTIONS requests
    $app->mapOptions([
        '/login' => ['POST'],
        '/token/{token}' => ['GET'],
        '/sources' => ['GET'],
        '/sources/{source}' => ['GET'],
        '/people/{id}' => ['GET'],
    ]);
};
