<?php

use Branchology\Api\Application\Application;

require '../vendor/autoload.php';

$app = new Application(['debug' => true]);
$app->load(require 'services/index.php');
$app->load(require 'middleware/index.php');
$app->load(require 'routes.php');

return $app;
