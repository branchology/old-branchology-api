<?php

use Branchology\Api\Application\Application;

return function (Application $app) {
    $app['allowed_origins'] = ['*'];

    if (getenv('TESTING')) {
        $app['testing-mode'] = true;
    }

    if (isset($app['testing-mode'])) {
        $app['db.config'] = require __DIR__ . '/../../config/db.test.php';
    } else {
        $app['db.config'] = require __DIR__ . '/../../config/db.local.php';
    }
};
