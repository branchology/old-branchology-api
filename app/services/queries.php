<?php

use Branchology\Api\Application\Application;
use Branchology\Persistence\Query\PeopleQuery;
use Branchology\Persistence\Query\SourceQuery;
use Branchology\Persistence\Query\TokenQuery;
use Branchology\Persistence\Query\UserQuery;

return function (Application $app) {
    $app['people.query'] = function ($app) {
        return new PeopleQuery($app['orm.entity-manager']);
    };

    $app['source.query'] = function ($app) {
        return new SourceQuery($app['orm.entity-manager']);
    };

    $app['token.query'] = function ($app) {
        return new TokenQuery($app['orm.entity-manager']);
    };

    $app['user.query'] = function ($app) {
        return new UserQuery($app['orm.entity-manager']);
    };
};
