<?php

use Branchology\Api\Application\Application;
use Branchology\Api\Hydrator;

return function (Application $app) {
    $app['source.hydrator'] = $app->share(function ($app) {
        return new Hydrator\SourceHydrator($app['source.repository']);
    });

    $app['token.hydrator'] = $app->share(function ($app) {
        return new Hydrator\TokenHydrator($app['token.repository']);
    });

    $app['person.hydrator'] = $app->share(function ($app) {
        return new Hydrator\People\PeopleHydrator($app['person.repository']);
    });
};
