<?php

use Branchology\Api\Application\Application;
use Branchology\Api\Converter\RepositoryBasedConverter;

return function (Application $app) {
    $app['person.converter'] = $app->share(function ($app) {
        return new RepositoryBasedConverter($app['person.repository']);
    });

    $app['source.converter'] = $app->share(function ($app) {
        return new RepositoryBasedConverter($app['source.repository']);
    });

    $app['token.converter'] = $app->share(function ($app) {
        return new RepositoryBasedConverter($app['token.repository']);
    });
};
