<?php

use Branchology\Api\Application\Application;
use Branchology\Persistence\Repository\PersonRepository;
use Branchology\Persistence\Repository\SourceRepository;
use Branchology\Persistence\Repository\TokenRepository;

return function (Application $app) {
    $app['person.repository'] = $app->share(function ($app) {
        return new PersonRepository($app['orm.entity-manager']);
    });

    $app['source.repository'] = $app->share(function ($app) {
        return new SourceRepository($app['orm.entity-manager']);
    });

    $app['token.repository'] = $app->share(function ($app) {
        return new TokenRepository($app['orm.entity-manager']);
    });
};
