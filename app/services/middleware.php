<?php

use Branchology\Api\Application\Application;
use Branchology\Api\Middleware\Authorization;

return function (Application $app) {
    $app['authorization.service'] = $app->share(function ($app) {
        return new Authorization($app['token.query']);
    });
};
