<?php

use Branchology\Api\Application\Application;
use Branchology\Api\Controller\LoginController;
use Branchology\Api\Controller\PeopleController;
use Branchology\Api\Controller\PeopleEventController;
use Branchology\Api\Controller\SourceController;
use Branchology\Api\Controller\TokenController;
use Branchology\Domain\Service\TokenFactory;

return function (Application $app) {
    $app['login.controller'] = $app->share(function ($app) {
        return new LoginController($app['user.query'], new TokenFactory());
    });

    $app['people.controller'] = $app->share(function ($app) {
        return new PeopleController($app['people.query']);
    });

    $app['people-event.controller'] = $app->share(function ($app) {
        return new PeopleEventController();
    });

    $app['source.controller'] = $app->share(function ($app) {
        return new SourceController($app['source.query']);
    });

    $app['token.controller'] = $app->share(function ($app) {
        return new TokenController();
    });
};
