<?php

use Branchology\Api\Application\Application;
use Branchology\Persistence\EntityManagerFactory;
use Branchology\Persistence\ObjectPersister;

return function (Application $app) {
    $app['orm.entity-manager'] = $app->share(function ($app) {
        return (new EntityManagerFactory())->create($app['db.config']);
    });

    $app['orm.persister'] = $app->share(function($app) {
        return new ObjectPersister($app['orm.entity-manager']);
    });
};
