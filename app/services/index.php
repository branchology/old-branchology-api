<?php

use Asm89\Stack\CorsService;
use Branchology\Api\Application\Application;
use Branchology\Api\Http\Request;
use Halpert\Http\Response\HalJsonResponse;
use Symfony\Component\HttpFoundation\Response;

return function (Application $app) {
    $app->load(require 'config.php');
    $app->load(require 'entity-manager.php');
    $app->load(require 'repositories.php');
    $app->load(require 'queries.php');
    $app->load(require 'controllers.php');
    $app->load(require 'hydrators.php');
    $app->load(require 'converters.php');
    $app->load(require 'middleware.php');

    $app['cors'] = function ($app) {
        return new CorsService([
            'allowedHeaders' => ['*'],
            'allowedMethods' => ['*'],
            'allowedOrigins' => $app['allowed_origins'],
            'exposedHeaders' => false,
            'maxAge' => false,
            'supportsCredentials' => false,
        ]);
    };

    // todo fixme stupid
    $app->after(function (Request $request, Response $response) {
        if ($response instanceof HalJsonResponse) {
            $response->setData(); // retrigger the data transformation
        }
    });
};
