<?php

use Branchology\Api\Application\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

return function (Application $app) {
    $app->after(function (Request $request, Response $response) use ($app) {
        $cors = $app['cors'];
        if (!$cors->isCorsRequest($request)) {
            return $response;
        }

        if (!$cors->isActualRequestAllowed($request)) {
            return new Response('Not allowed.', 403);
        }

        if ($cors->isPreflightRequest($request)) {
            $corsResponse = $cors->handlePreflightRequest($request);
            foreach ($corsResponse->headers as $name => $value) {
                $response->headers->set($name, $value);
            }
        }

        return $cors->addActualRequestHeaders($response, $request);
    });
};
