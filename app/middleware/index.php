<?php

use Asm89\Stack\CorsService;
use Branchology\Api\Application\Application;
use Branchology\Api\Middleware\JsonBodyConverter;

return function (Application $app) {
    $app->load(require 'cors.php');

    $app['json-body.middleware'] = $app->share(function () {
        return new JsonBodyConverter();
    });

    $app->before('json-body.middleware:convert');
};
