<?php

use Branchology\Persistence\EntityManagerFactory;
use Branchology\Test\FixtureIdentityMap;
use Evenement\EventEmitterInterface;
use Nelmio\Alice\Fixtures;
use Peridot\Console\Environment;
use Peridot\Plugin\Doctrine\DoctrinePlugin;
use Peridot\Plugin\Doctrine\EntityManager\EntityManagerService;
use Peridot\Plugin\Doctrine\EntityManager\SchemaService;
use Peridot\Plugin\Prophecy\ProphecyPlugin;
use Peridot\Plugin\Watcher\WatcherPlugin;

return function (EventEmitterInterface $eventEmitter) {
    (new WatcherPlugin($eventEmitter))->track(__DIR__ . '/src');
    new ProphecyPlugin($eventEmitter);

    $eventEmitter->on('peridot.start', function (Environment $environment) {
        $environment->getDefinition()->getArgument('path')->setDefault('specs');
    });

    $managerService = new EntityManagerService($eventEmitter);
    $managerService->setEntityManagerFactory(function () {
        return (new EntityManagerFactory())->create(require __DIR__ . '/config/db.test.php');
    });
    new DoctrinePlugin($eventEmitter, $managerService);

    $eventEmitter->on('runner.start', function () use($managerService) {
        $entityManager = $managerService->createEntityManager();
        $schemaService = new SchemaService($entityManager);
        $schemaService->dropDatabase()->createDatabase();

        $objects = Fixtures::load(require __DIR__ . '/config/fixtures.php', $entityManager);

        FixtureIdentityMap::loadIdentityMap($objects);

        copy(__DIR__ . '/tmp/test.db', __DIR__ . '/tmp/clean.db');
    });

    $eventEmitter->on('doctrine.entityManager.preCreate', function () {
        copy(__DIR__ . '/tmp/clean.db', __DIR__ . '/tmp/test.db');
    });

    $eventEmitter->on('peridot.end', function () {
        foreach (glob(__DIR__ . '/tmp/*.db') as $db) {
            unlink($db);
        }
    });
};
